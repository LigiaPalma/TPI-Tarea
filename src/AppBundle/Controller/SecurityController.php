<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{

  /**
   * @Route("/login", name="login")
   */
  public function loginAction(AuthenticationUtils $authenticationUtils)
  {

    // get the login error if there is one
     $error = $authenticationUtils->getLastAuthenticationError();

     // last username entered by the user
     $lastUsername = $authenticationUtils->getLastUsername();

     return $this->render('login/login.html.twig', [
         'last_username' => $lastUsername,
         'error'         => $error,
     ]);

  }

  /**
   * @Route("/registrar_usuario", name="registrar_usuario")
   */
  public function registrarUsuarioAction()
  {

    return $this->render('login/signin.html.twig');
  }


  /**
   * @Route("consultar_usuario_existente", name="consultar_usuario_existente")
   */
  public function consultarUsuarioAction(Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      if ($request->isMethod('POST')) {
          // recuperar request
          $nombreUsuario = $request->get('usuario');
          $email = $request->get('correo');

          //conexion a la BD mediante doctrine
          $em = $this->getDoctrine()->getManager();

          //consulta
          $query = $em->createQuery(
                         'SELECT u
                         FROM AppBundle:Usuario u
                         WHERE u.nombreUsuario =:nombre
                         OR u.email = :email')
                      ->setParameter('nombre', $nombreUsuario )
                      ->setParameter('email',  $email );
          $usuario = $query->execute();


          if ($usuario) {
            if ($usuario->getEmail() == $email ) {
                return new Response("email");
            } else {
              return new Response("username");
            }

              $usuario->setIsactive(1);
          } else {
            return new Response("null");

          }
      }

      return $this->redirectToRoute('usuario_create');
  }

  /**
   * @Route("usuario_create", name="usuario_create")
   */
  public function usuarioCreateAction(Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      if ($request->isMethod('POST')) {

        try {

          $usuario = new Usuario();
          $usuario->setNombre($request->get('nombre'));
          $usuario->setApellido($request->get('apellido'));
          $usuario->setEmail($request->get('email'));
          $usuario->setNombreUsuario($request->get('usuario'));

          $clave = password_hash($request->get('clave'), PASSWORD_BCRYPT, ['cost' => 4]);
          $usuario->setPassword($clave);

          $tipoUsuario =$em->getRepository("AppBundle:TipoUsuario")->find(2);
          $usuario->setIdTipoUsuario($tipoUsuario);

          $usuario->setActivo(true);

          $usuario->setFechaCreacion(new \DateTime('now'));
          $usuario->setFechaModificacion(new \DateTime('now'));

          $em->persist($usuario);
          $em->flush();

          return $this->redirectToRoute('login');

        } catch (\Exception $e) {
          return $e;
        }



      }

      return $this->redirectToRoute('usuario_create');
  }

  /**
   * @Route("/control", name="control_redirect")
   */
  public function controlRedirectAction()
  {
        return $this->redirectToRoute('formularios');
  }

  /**
   * @Route("/login_check", name="login_check")
   */
  public function loginCheckAction()
  {
  }

  /**
   * @Route("/logout", name="logout")
   */
  public function logoutCheckAction()
  {
  }


}
