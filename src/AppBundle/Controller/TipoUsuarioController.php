<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TipoUsuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Tipousuario controller.
 *
 * @Route("tipousuario")
 */
class TipoUsuarioController extends Controller
{
    /**
     * Lists all tipoUsuario entities.
     *
     * @Route("/", name="tipousuario_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tipoUsuarios = $em->getRepository('AppBundle:TipoUsuario')->findAll();

        return $this->render('tipousuario/index.html.twig', array(
            'tipoUsuarios' => $tipoUsuarios,
        ));
    }

    /**
     * Creates a new tipoUsuario entity.
     *
     * @Route("/new", name="tipousuario_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tipoUsuario = new Tipousuario();
        $form = $this->createForm('AppBundle\Form\TipoUsuarioType', $tipoUsuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tipoUsuario);
            $em->flush();

            return $this->redirectToRoute('tipousuario_show', array('idTipoUsuario' => $tipoUsuario->getIdtipousuario()));
        }

        return $this->render('tipousuario/new.html.twig', array(
            'tipoUsuario' => $tipoUsuario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tipoUsuario entity.
     *
     * @Route("/{idTipoUsuario}", name="tipousuario_show")
     * @Method("GET")
     */
    public function showAction(TipoUsuario $tipoUsuario)
    {
        $deleteForm = $this->createDeleteForm($tipoUsuario);

        return $this->render('tipousuario/show.html.twig', array(
            'tipoUsuario' => $tipoUsuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tipoUsuario entity.
     *
     * @Route("/{idTipoUsuario}/edit", name="tipousuario_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TipoUsuario $tipoUsuario)
    {
        $deleteForm = $this->createDeleteForm($tipoUsuario);
        $editForm = $this->createForm('AppBundle\Form\TipoUsuarioType', $tipoUsuario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tipousuario_edit', array('idTipoUsuario' => $tipoUsuario->getIdtipousuario()));
        }

        return $this->render('tipousuario/edit.html.twig', array(
            'tipoUsuario' => $tipoUsuario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tipoUsuario entity.
     *
     * @Route("/{idTipoUsuario}", name="tipousuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TipoUsuario $tipoUsuario)
    {
        $form = $this->createDeleteForm($tipoUsuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tipoUsuario);
            $em->flush();
        }

        return $this->redirectToRoute('tipousuario_index');
    }

    /**
     * Creates a form to delete a tipoUsuario entity.
     *
     * @param TipoUsuario $tipoUsuario The tipoUsuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TipoUsuario $tipoUsuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipousuario_delete', array('idTipoUsuario' => $tipoUsuario->getIdtipousuario())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
