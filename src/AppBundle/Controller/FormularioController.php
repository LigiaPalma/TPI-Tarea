<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Entity\Documento;
use AppBundle\Entity\AtributoDocumento;


class FormularioController extends Controller
{

  /********************************************** FUNCIONES PRIVADAS  ****************************************/


  /*********************************************** MÉTODOS PÚBLICOS   ****************************************/

  /**
  * @Route("/", name="formularios")
  */
  public function formulariosActualesAction()
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {

      return $this->render('formulario/dashboard_formularios.html.twig');
    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }

  /**
  * @Route("/busqueda_documento/", name="busqueda_documento")
  */
  public function busquedaDocumentoAction()
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {
          $usuario = $this->getUser();
       
         
         //conexion a la BD mediante doctrine
         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery(
              "SELECT d
               FROM AppBundle:Documento d, AppBundle:Documentocategoria dc, AppBundle:Tipodocumento td
                WHERE d.categoria = dc.id
                   AND dc.idTipoDocumento = td.id
                   AND d.usuario <> :idUsuario")
           ->setParameter("idUsuario", $usuario->getIdUsuario());
           $documentos = $query->getResult();
          


           return $this->render('formulario/busqueda_documento.html.twig',array(
            "documento" => $documentos));
       
      
    
    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }

  /**
  * @Route("/formularios/all", name="formularios_all")
  */
  public function formulariosActualesPublicosAction()
  {

      return $this->render('formulario/formularios_actuales.html.twig');

  }

  /**
  * @Route("/nuevo_formulario", name="nuevo_formulario")
  */
  public function nuevoFormularioAction()
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();

      $tipoDocumento = $em->getRepository('AppBundle:Tipodocumento')->findAll();

      return $this->render('formulario/nuevo_formulario.html.twig' , array(
        "tipoDocumento" => $tipoDocumento,
      ));
    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }

  /**
  * @Route("/documento_por_categoria/{id}", name="documento_por_categoria")
  */
  public function documentoPorCategoriaAction($id)
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();

      $documento = $em->getRepository('AppBundle:Documento')->find($id);

      return $this->render('formulario/documento_por_entidad.html.twig' , array(
        "documento" => $documento,
      ));
    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }

  /**
  * @Route("/all_plantillas", name="all_plantillas")
  */
  public function allPlantillasAction()
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();

      $documentos = $em->getRepository('AppBundle:Documento')->findAll();

      return $this->render('formulario/formularios_actuales.html.twig' , array(
        "documento" => $documentos,
      ));
    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }

  /**
  * @Route("/mis_documentos/{id}", name="mis_documentos")
  */
  public function misDocumentosAction($id)
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();

      switch ($id) {
          case 1:
          // Profesionales
          $query = $em->createQuery(
              "SELECT d
               FROM AppBundle:Documento d, AppBundle:Documentocategoria dc, AppBundle:Tipodocumento td
                WHERE d.categoria = dc.id
                   AND dc.idTipoDocumento = td.id
                   AND td.id = 1
                   AND d.usuario = :idUsuario")
           ->setParameter("idUsuario", $usuario->getIdUsuario());
          $documentos = $query->getResult();

          break;

          case 2:
          // Particulares
          $query = $em->createQuery(
              "SELECT d
               FROM AppBundle:Documento d, AppBundle:Documentocategoria dc, AppBundle:Tipodocumento td
                WHERE d.categoria = dc.id
                   AND dc.idTipoDocumento = td.id
                   AND td.id = 2
                   AND d.usuario = :idUsuario")
           ->setParameter("idUsuario", $usuario->getIdUsuario());
          $documentos = $query->getResult();

          break;

          case 3:
          // Todos
          $documentos = $em->getRepository('AppBundle:Documento')->findByUsuario(
            array("usuario" => $usuario->getIdUsuario())
          );

          break;
      }

      return $this->render('formulario/documentos_actuales.html.twig' , array(
        "documento" => $documentos,
        "id" => $id
      ));


    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }

  /**
  * @Route("/plantillas_existentes/{id}", name="plantillas_existentes")
  */
  public function plantillasExistentesAction($id)
  {
    if (($this->isGranted('ROLE_admin')) or ($this->isGranted('ROLE_administrador')) or ($this->isGranted('ROLE_cliente'))) {

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();

      switch ($id) {
          case 1:
          // Profesionales
          $query = $em->createQuery(
              "SELECT d
               FROM AppBundle:Usuario u,
                    AppBundle:TipoUsuario tu,
                    AppBundle:Documento d,
                    AppBundle:Documentocategoria dc,
                    AppBundle:Tipodocumento td
                WHERE d.usuario = u.idUsuario
                   AND u.idTipoUsuario = tu.idTipoUsuario
                   AND d.categoria = dc.id
                   AND dc.idTipoDocumento = td.id
                   AND td.id = 1
                   AND tu.idTipoUsuario = 1 ");
          $documentos = $query->getResult();

          break;

          case 2:
          // Particulares
          $query = $em->createQuery(
              "SELECT d
               FROM AppBundle:Usuario u,
                    AppBundle:TipoUsuario tu,
                    AppBundle:Documento d,
                    AppBundle:Documentocategoria dc,
                    AppBundle:Tipodocumento td
                WHERE d.usuario = u.idUsuario
                   AND u.idTipoUsuario = tu.idTipoUsuario
                   AND d.categoria = dc.id
                   AND dc.idTipoDocumento = td.id
                   AND td.id = 2
                   AND tu.idTipoUsuario = 1 ");
          $documentos = $query->getResult();

          break;

          case 3:
          // Todos
          $query = $em->createQuery(
              "SELECT d
               FROM AppBundle:Usuario u,
                    AppBundle:TipoUsuario tu,
                    AppBundle:Documento d
                WHERE d.usuario = u.idUsuario
                   AND u.idTipoUsuario = tu.idTipoUsuario
                   AND tu.idTipoUsuario = 1 ");
          $documentos = $query->getResult();

          break;
      }

      return $this->render('formulario/plantillas_actuales.html.twig' , array(
        "documento" => $documentos,
        "id" => $id
      ));


    }
    throw $this->createNotFoundException( 'Permiso denegado' );

  }


  /**
   * @Route("plantilla_create_usuario", name="plantilla_create_usuario")
   */
  public function plantillaUsuarioCreateAction(Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      if ($request->isMethod('POST')) {

        try {
          //obteniendo los parametros de request
          $idDocumento = $request->get("id_documento");

          //conexion a la BD mediante doctrine
          $em = $this->getDoctrine()->getManager();

          $documentoPlantilla = $em->getRepository("AppBundle:Documento")->find($idDocucmento);

          $documentoNuevo = new Doccumento();
          $documentoNuevo->setUsuario($this->getUser());
          $documentoNuevo->setFecha(new \DateTime('now'));


          $documentoNuevo->setTitutlo($documentoPlantilla->getNombre());



          $em->persist($documentoNuevo);
          $em->flush();

          return new Response($documentoNuevo->getId());

        } catch (\Exception $e) {
          return $e;
        }



      }

      return $this->redirectToRoute('usuario_create');
  }

  /**
   * @Route("iniciar_nuevo_documento", name="iniciar_nuevo_documento")
   */
  public function iniciarNuevoDocumentoAction(Request $request)
  {

      $em = $this->getDoctrine()->getManager();

      //obteniendo los parametros de request
      $idDocumento = $request->get("idDocumento");

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();
      if ($idDocumento != 0) {
        $documentoNuevo = $em->getRepository("AppBundle:Documento")->find($idDocucmento);
      }else {
        $documentoNuevo = new Documento();
      }

      $documentoNuevo->setTitulo($request->get("nombreDocumento"));
      $documentoNuevo->setDescripcion($request->get("descripcion"));
      $categoria = $em->getRepository("AppBundle:Documentocategoria")->find($request->get("categoria"));
      $documentoNuevo->setCategoria($categoria);
      $documentoNuevo->setCompleto(false);
      $documentoNuevo->setUsuario($this->getUser());
      $documentoNuevo->setFecha(new \DateTime('now'));

      $em->persist($documentoNuevo);
      $em->flush();

      $encoders = array(new JsonEncoder());
      $normalizer = array(new ObjectNormalizer());
      $serializer = new Serializer($normalizer, $encoders);
      $json = $serializer->serialize($documentoNuevo, 'json');

      return new JsonResponse($json);

  }

  /**
   * @Route("atributos_nuevo_documento", name="atributos_nuevo_documento")
   */
  public function atributosNuevoDocumentoAction(Request $request)
  {


      //obteniendo los parametros de request
      $idDocumento = $request->get("id_documento");
      $listaAtributos = $request->get("lista_atributos");

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();

      $documento = $em->getRepository("AppBundle:Documento")->find($idDocumento);
      $documento->setCompleto(false);
      $em->persist($documento);
      $em->flush();

      foreach ($listaAtributos as $atributo) {

        $nuevoAtributo = new AtributoDocumento();
        $nuevoAtributo->setNombreAtributo($atributo["nombreAtributo"]);
        $nuevoAtributo->setTextoAntes($atributo["textoAntesAtributo"]);
        $nuevoAtributo->setTextoDespues($atributo["textoDespuesAtributo"]);
        $nuevoAtributo->setIdDocumento($documento);
        $nuevoAtributo->setFechaCreacion(new \DateTime('now'));
        $nuevoAtributo->setFechaModificacion(new \DateTime('now'));
        $em->persist($nuevoAtributo);
        $em->flush();

      }


      return new JsonResponse("OK");

  }


}
