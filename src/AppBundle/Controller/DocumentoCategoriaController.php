<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentoCategoriaController extends Controller
{

  /**
   * @Route("categoria_tipo_documento", name="categoria_documento")
   */
  public function DocumentoCategoraTipoAction(Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      //obteniendo los parametros de request
      $idTipoDocumento = $request->get("tipoDocumento");

      //conexion a la BD mediante doctrine
      $em = $this->getDoctrine()->getManager();

      $categorias = $em->getRepository("AppBundle:Documentocategoria")->findByIdTipoDocumento($idTipoDocumento);

      $respuesta = "";
        $respuesta = $respuesta."<option value='0' selected>Selecciona un Categoría  </option>";
      foreach ($categorias as $c ) {
          $respuesta = $respuesta.
          "<option value='".$c->getId()."'>".
            $c->getNombre()
          ."</option>";
      }

      return new Response($respuesta);
   }


}
