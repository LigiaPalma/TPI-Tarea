<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentoCategoria
 *
 * @ORM\Table(name="documento_categoria", indexes={@ORM\Index(name="documentocategoria_idx", columns={"id_tipo_documento"})})
 * @ORM\Entity
 */
class DocumentoCategoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre = 'NULL';

    /**
     * @var \Tipodocumento
     *
     * @ORM\ManyToOne(targetEntity="Tipodocumento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_documento", referencedColumnName="id")
     * })
     */
    private $idTipoDocumento;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return DocumentoCategoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idTipoDocumento
     *
     * @param \AppBundle\Entity\Tipodocumento $idTipoDocumento
     *
     * @return DocumentoCategoria
     */
    public function setIdTipoDocumento(\AppBundle\Entity\Tipodocumento $idTipoDocumento = null)
    {
        $this->idTipoDocumento = $idTipoDocumento;

        return $this;
    }

    /**
     * Get idTipoDocumento
     *
     * @return \AppBundle\Entity\Tipodocumento
     */
    public function getIdTipoDocumento()
    {
        return $this->idTipoDocumento;
    }
}
