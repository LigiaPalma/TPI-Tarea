<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtributoDocumento
 *
 * @ORM\Table(name="atributo_documento", indexes={@ORM\Index(name="documento", columns={"id_documento"})})
 * @ORM\Entity
 */
class AtributoDocumento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_atributo", type="string", length=100, nullable=false)
     */
    private $nombreAtributo;

    /**
     * @var string
     *
     * @ORM\Column(name="texto_antes", type="text", length=65535, nullable=false)
     */
    private $textoAntes;

    /**
     * @var string
     *
     * @ORM\Column(name="texto_despues", type="text", length=65535, nullable=false)
     */
    private $textoDespues;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false)
     */
    private $fechaCreacion = 'current_timestamp()';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="datetime", nullable=false)
     */
    private $fechaModificacion = 'current_timestamp()';

    /**
     * @var \Documento
     *
     * @ORM\ManyToOne(targetEntity="Documento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_documento", referencedColumnName="id")
     * })
     */
    private $idDocumento;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreAtributo
     *
     * @param string $nombreAtributo
     *
     * @return AtributoDocumento
     */
    public function setNombreAtributo($nombreAtributo)
    {
        $this->nombreAtributo = $nombreAtributo;

        return $this;
    }

    /**
     * Get nombreAtributo
     *
     * @return string
     */
    public function getNombreAtributo()
    {
        return $this->nombreAtributo;
    }

    /**
     * Set textoAntes
     *
     * @param string $textoAntes
     *
     * @return AtributoDocumento
     */
    public function setTextoAntes($textoAntes)
    {
        $this->textoAntes = $textoAntes;

        return $this;
    }

    /**
     * Get textoAntes
     *
     * @return string
     */
    public function getTextoAntes()
    {
        return $this->textoAntes;
    }

    /**
     * Set textoDespues
     *
     * @param string $textoDespues
     *
     * @return AtributoDocumento
     */
    public function setTextoDespues($textoDespues)
    {
        $this->textoDespues = $textoDespues;

        return $this;
    }

    /**
     * Get textoDespues
     *
     * @return string
     */
    public function getTextoDespues()
    {
        return $this->textoDespues;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return AtributoDocumento
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return AtributoDocumento
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set idDocumento
     *
     * @param \AppBundle\Entity\Documento $idDocumento
     *
     * @return AtributoDocumento
     */
    public function setIdDocumento(\AppBundle\Entity\Documento $idDocumento = null)
    {
        $this->idDocumento = $idDocumento;

        return $this;
    }

    /**
     * Get idDocumento
     *
     * @return \AppBundle\Entity\Documento
     */
    public function getIdDocumento()
    {
        return $this->idDocumento;
    }
}
