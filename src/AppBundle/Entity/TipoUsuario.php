<?php

namespace AppBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\Role\Role;
use Doctrine\ORM\Mapping as ORM;

/**
 * TipoUsuario
 *
 * @ORM\Table(name="tipo_usuario")
 * @ORM\Entity
 */
class TipoUsuario  extends Role
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_usuario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipoUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="rol_tipo_usuario", type="string", length=64, nullable=false)
     */
    private $rolTipoUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=64, nullable=true)
     */
    private $descripcion = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false)
     */
    private $fechaCreacion = 'current_timestamp()';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="datetime", nullable=false)
     */
    private $fechaModificacion = 'current_timestamp()';



    /**
     * Get idTipoUsuario
     *
     * @return integer
     */
    public function getIdTipoUsuario()
    {
        return $this->idTipoUsuario;
    }

    /**
     * Set rolTipoUsuario
     *
     * @param string $rolTipoUsuario
     *
     * @return TipoUsuario
     */
    public function setRolTipoUsuario($rolTipoUsuario)
    {
        $this->rolTipoUsuario = $rolTipoUsuario;

        return $this;
    }

    /**
     * Get rolTipoUsuario
     *
     * @return string
     */
    public function getRolTipoUsuario()
    {
        return $this->rolTipoUsuario;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoUsuario
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return TipoUsuario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return TipoUsuario
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return TipoUsuario
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    public function getRole()
    {
        return "ROLE_".$this->rolTipoUsuario."";
    }
}
