INSERT INTO `bd_tpi_tarea`.`tipo_usuario` (`rol_tipo_usuario`, `activo`) VALUES ('admin', '1');
INSERT INTO `bd_tpi_tarea`.`tipo_usuario` (`rol_tipo_usuario`, `activo`) VALUES ('cliente', '1');


-- https://bcrypt-generator.com/   N° vueltas: 12 
-- contraseña: estudiar+
-- contraseña encriptada: $2y$12$nUbZJVjobVOn9IdFs2Qi5./TFI0FtXyEfhkTMuHpNLBw39ywloK/u

INSERT INTO `bd_tpi_tarea`.`usuario` (`nombre_usuario`, `password`, `email`, `id_tipo_usuario`, `activo`) VALUES ('tpi115', '$2y$12$IH0zLER6qb.dDK543VtC4uYLoCKoqxbvEjp1ptbYk4Ta44PmGDNIi', 'admin@gmail.com', '1', '1');


INSERT INTO `bd_tpi_tarea`.`tipodocumento` (`nombre`) VALUES ('Profesional');
INSERT INTO `bd_tpi_tarea`.`tipodocumento` (`nombre`) VALUES ('Particular');

INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Recursos humanos, derecho laboral');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Contratos de trabajo');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Funcionamiento legal de la empresa');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Creación de empresas');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Fiscalidad, tasas e impuestos');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Propiedad intelectual y nuevas tecnologías');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Actividad comercial');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('1', 'Sector inmobiliario profesional');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Trabajo');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Solicitud de vacaciones, excedencias y bajas');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Tercera edad, medicina, sanidad');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Alojamiento, inmobiliario');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Gestiones arrendamientos inmobiliarios');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Comunidad de propietarios');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Vida familiar, matrimonio, divorcio');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Vida diaria');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Gestiones administrativas');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Procedimientos legales');
INSERT INTO `bd_tpi_tarea`.`documentocategoria` (`id_tipo_documento`, `nombre`) VALUES ('2', 'Consumo');


INSERT INTO `bd_tpi_tarea`.`documento` (`titulo`, `categoria`, `descripcion`, `fecha`, `usuario`) VALUES ('Contrato de compraventa de vehículo de segunda mano entre particulares', '16', 'Este documento es un contrato que permite que el propietario (ya sea una persona física o una persona jurídica como una empresa) de un vehículo automóvil (coche, moto, furgoneta, etc) usado venda el mismo a otra persona (ya sea una persona física o una persona jurídica como una empresa). Por lo tanto, este documento determina las condiciones de la compraventa del vehículo y su cambio de titularidad', '2019-11-16', '1');


UPDATE bd_tpi_tarea.usuario
SET
password = "$2y$12$IH0zLER6qb.dDK543VtC4uYLoCKoqxbvEjp1ptbYk4Ta44PmGDNIi";