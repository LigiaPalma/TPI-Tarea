SET FOREIGN_KEY_CHECKS = 0;
-- SET FOREIGN_KEY_CHECKS = 1;

SET SQL_SAFE_UPDATES = 0;
-- SET SQL_SAFE_UPDATES = 1;

/**************************** TIPO USUARIO ********************************/
DROP TABLE IF EXISTS `tipo_usuario`;
CREATE TABLE `tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol_tipo_usuario` VARCHAR(64)  NOT NULL,
  `descripcion` VARCHAR(64)  DEFAULT NULL,
  `activo` TINYINT(1) NOT NULL,
  `fecha_creacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de Creación del Registro',
  `fecha_modificacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Fecha y Hora que indica el momento en que se modifico algún valor de este registro',
  PRIMARY KEY (`id_tipo_usuario`)
) ;


/**************** USUARIO ********************** */

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(64)  NOT NULL ,
  `apellido` VARCHAR(64)  NOT NULL ,
  `nombre_usuario` VARCHAR(64)  NOT NULL UNIQUE,
  `password` VARCHAR(64) NOT NULL,
  `email` VARCHAR(64)  DEFAULT NULL,
  `id_tipo_usuario` INT(11) NOT NULL,
  `activo` TINYINT(1) NOT NULL,
  `fecha_creacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de Creación del Registro',
  `fecha_modificacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Fecha y Hora que indica el momento en que se modifico algún valor de este registro',
  PRIMARY KEY (`id_usuario`)
) ;

ALTER TABLE usuario
ADD FOREIGN KEY (id_tipo_usuario) REFERENCES tipo_usuario(id_tipo_usuario);




CREATE TABLE `bd_tpi_tarea`.`documento_categoria` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `bd_tpi_tarea`.`tipo_documento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `bd_tpi_tarea`.`documento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NULL,
  `categoria` INT NULL,
  `descripcion` VARCHAR(45) NULL,
  `fecha` DATETIME NULL,
  `usuario` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `Documento-Usuario`
    FOREIGN KEY (`usuario`)
    REFERENCES `bd_tpi_tarea`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Documento-DocumentoCategoria`
    FOREIGN KEY (`categoria`)
    REFERENCES `bd_tpi_tarea`.`documento_categoria` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `bd_tpi_tarea`.`documento_categoria` 
ADD COLUMN `id_tipo_documento` INT(11) NOT NULL AFTER `id`;


ALTER TABLE `bd_tpi_tarea`.`documento_categoria` 
ADD INDEX `documentocategoria_idx` (`id_tipo_documento` ASC);
ALTER TABLE `bd_tpi_tarea`.`documento_categoria` 
ADD CONSTRAINT `documento_categoria`
  FOREIGN KEY (`id_tipo_documento`)
  REFERENCES `bd_tpi_tarea`.`tipodocumento` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;



ALTER TABLE `bd_tpi_tarea`.`documento` 
CHANGE COLUMN `descripcion` `descripcion` VARCHAR(255) NULL ;

ALTER TABLE `bd_tpi_tarea`.`documento` 
ADD COLUMN `completo` TINYINT(1) NULL DEFAULT NULL AFTER `usuario`;


CREATE TABLE `bd_tpi_tarea`.`atributo_documento` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_documento` INT(11) NOT NULL,
  `nombre_atributo` VARCHAR(100)  NOT NULL,
  `texto_antes` TEXT NOT NULL,
  `texto_despues` TEXT NOT NULL,
  `fecha_creacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de Creación del Registro',
  `fecha_modificacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Fecha y Hora que indica el momento en que se modifico algún valor de este registro',
  PRIMARY KEY (`id`),
  CONSTRAINT `documento`
  FOREIGN KEY (`id_documento`)
  REFERENCES `bd_tpi_tarea`.`documento` (`id`)
  ON DELETE RESTRICT
  ON UPDATE NO ACTION
);

