    $(document).ready(function () {

        jQuery.validator.addMethod("txtnumber",function (value,element) {
            return this.optional( element ) || /^^[0-9a-zA-Z_áéíóúñ]*$/.test( value );
        },"Este valor es incorrecto, unicamente permite letras y números");
        jQuery.validator.addMethod("txt",function (value,element) {
          return this.optional( element ) || /^\s*([a-zA-Z_áéíóúñ]+\s?){1,3}$/.test( value );
        },"Este valor es incorrecto, unicamente permite entre 1 a 3 palabras y un espacio por cada palabra.");
        jQuery.validator.addMethod("email",function (value,element) {
            return this.optional( element ) || /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        },"Este valor es incorrecto, unicamente permite letras y números");
        jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
            return arg != value;
        }, "Por favor Elija un Opción.");

        var form = $("#formulario");
        form.validate({
            rules: {

                nombre: {required: true, txt: true},
                apellido: {required: true, txt: true},
                email: {required: true, email: true},
                usuario: {required: true, txtnumber: true},
                clave: {required: true, txtnumber: true, minlength: 8},
                clave2: {required: true, txtnumber: true, minlength: 8},
            },
            messages: {

                nombre: {
                    required: "El Nombre es Obligatorio",
                    txt: "El nombre está formado únicamente por letras"
                },
                apellido: {
                    required: "El Apellido es Obligatorio",
                    txt: "El apellido está formado únicamente por letras"
                },
                email: {
                    required: "El Correo Electrónico es Obligatorio",
                    txt: "No es un correo válido"
                },
                usuario: {
                    required: "El Usuario es Obligatorio",
                    txtnumber: "Un Usuario valido está formado únicamente por letras, numeros y sin espacios"
                },
                clave: {
                    required: "La Clave es Obligatoria",
                    txtnumber: "Una Clave valida está formada únicamente por letras, numeros y sin espacios"
                },
                clave2: {
                    required: "La Clave es Obligatoria",
                    txtnumber: "Una Clave valida está formada únicamente por letras, numeros y sin espacios"
                },
            },
            errorPlacement: function errorPlacement(error, element) {

                if(element.attr("name")=="nombre"){
                    error.appendTo('#errorNombre');
                }
                if(element.attr("name")=="apellido"){
                    error.appendTo('#errorApellido');
                }
                if(element.attr("name")=="email"){
                    error.appendTo('#errorEmail');
                }
                if(element.attr("name")=="usuario"){
                    error.appendTo('#errorUsuario');
                }
                if(element.attr("name")=="clave"){
                    error.appendTo('#errorClave');
                }
                if(element.attr("name")=="clave2"){
                    error.appendTo('#errorClave2');
                }
            }
        });

        jQuery.extend(jQuery.validator.messages, {
            required: "Este campo es obligatorio",
            remote: "Por favor, rellena este campo",
            email: "Por favor, escribe una dirección de correo válida",
            url: "Por favor, escribe una URL válida",
            date: "Por favor, escribe una fecha válida",
            dateISO: "Por favor, escribe una fecha (ISO) válida",
            number: "Por favor, escribe un número entero válido",
            digits: "Por favor, escribe sólo dígitos",
            creditcard: "Por favor, escribe un número de tarjeta válido",
            equalTo: "Por favor, escribe el mismo valor de nuevo",
            accept: "Por favor, escribe un valor con una extensión aceptada",
            maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres"),
            minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres"),
            rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres"),
            range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}"),
            max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}"),
            min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}")
        });

    });
